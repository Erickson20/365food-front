
import swal from 'sweetalert';
import React from "react";
import swalModal from '@sweetalert/with-react';


 function fomatCategoryAndProduct(products,categories) {

    let familia = products.map( (data,i) => {
            let familia = categories.filter((options,i)=>{
                return options.codfamilia === data.codfamilia
            },data)
            return {
                id: data.codfamilia,
                category: familia[0].descripcion,
                products:[]
            }
        })
    let familiaNoRepeat = familia.filter((v,i,a)=>a.findIndex(t=>(t.category === v.category))===i)

    for (const key in familiaNoRepeat) {
        for (const i in products) {
            if(familiaNoRepeat[key].id === products[i].codfamilia){
                familiaNoRepeat[key].products.push(products[i])
            }
        }
    }

    return  familiaNoRepeat;

}



function Modal(content,title) {
     return swalModal({
         text: title,
         buttons: {
             cancel: "CANCELAR",
             save: "GUARDAR",
         },
         content: content

     })

}


function lineInvoice(products,idfactura){
        let body = {
            "idfactura": idfactura,
            "mostrar_cantidad": 1,
            "mostrar_precio": 1,
            "porcomision": 0,
            "actualizastock": parseInt(-Math.abs(products.count)),
            "cantidad": products.count,
            "descripcion": products.description,
            "dtopor": 0,
            "dtopor2": 0,
            "idproducto": products.idPropduct,
            "irpf": 0,
            "iva": 0,
            "pvpsindto": products.price,
            "pvptotal": products.price,
            "pvpunitario": products.price,
            "recargo": 0,
            "referencia": products.text,
            "servido": 0,
            "suplido": 0,
        }

        return formatDataForApi(body)
}

function setproducts(products){
     return products? Object.values(products).map((data,i)=>{
         return {
             cantidad:data.count,
             referencia:data.text,
             descripcion:data.description,
             idproducto:data.idproducto,
             precio:data.price,
             stock:data.stock,
             img:data.img
         }

     }) : []
}


function setProductTckings(products){
    console.log({products})
     let board = [
         {id:1, title: 'PENDIENTE', cards:[]},
         {id:1, title: 'EN PROCESO', cards:[]},
         {id:1, title: 'LISTO', cards:[]},
         {id:1, title: 'EN CAMINO', cards:[]},
         {id:1, title: 'ENTREGADO', cards:[]},
     ]

    for (const boardKey in board) {
        board[boardKey].id = parseInt(boardKey) + 1
        board[boardKey].cards = products.filter((data,i) =>
            data.status.toLowerCase() === board[boardKey].title.toLowerCase()
        )

    }
    return {columns:board};
}





function formatDataForApi(body) {
    const formData = new URLSearchParams();
    for (const bodyKey in body) {
        formData.append(bodyKey,body[bodyKey])
    }
    return formData

}

function nameForStatus(id) {

     let status = {
         1: 'Pendiente',
         2: 'En proceso',
         3: 'Listo',
         4: 'En camino',
         5: 'Entregado'
     }
     return status[id] ?? 'Pendiente';

}

function alert(opt,title,text) {

    let alertBody = swal({title: title, text: text, icon: opt === 'decision'? 'warning':opt, button: "ok",})
     let options = {
         success:  alertBody,
         warning:  alertBody,
         danger:  alertBody,
     }
     return options[opt] ?? swal({title: title, text: text, icon: "success", button: "ok",})

}


async function alertDecisions(opt,title,text,btnName) {

    let alertBody = swal({title: title, text: text, icon:opt, button: btnName,buttons: true, dangerMode: true,})
    let response = await alertBody.then((response)=> {
        if(response){
            swal("Bien Acccion ejecutada correctamente", {
                icon: "success",
            });
        }
        return response
    })
    return response
}



export {
    fomatCategoryAndProduct,
    alert,
    alertDecisions,
    Modal,
    formatDataForApi,
    lineInvoice,
    setproducts,
    setProductTckings,
    nameForStatus
}
