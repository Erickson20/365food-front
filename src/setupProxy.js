const { createProxyMiddleware } = require('http-proxy-middleware');
const cors = require('cors');
module.exports = function(app) {

    app.use(
        '/api',
        createProxyMiddleware({
            target: 'http://192.241.154.148:84',
            changeOrigin: true,
            pathRewrite: {
                "^/api": "/api/3"
            }
        })
    );

    app.use(
        'facturascripts/api',
        createProxyMiddleware({
            target: 'localhost:81',
            changeOrigin: true,
            pathRewrite: {
                "^/api": "/api/3"
            }
        })
    );
    app.use(
        '/api',
        createProxyMiddleware({
            target: 'https://order-pizza-api.herokuapp.com',
            changeOrigin: true,
            pathRewrite: {
                "^/api": "/api/3"
            }
        })
    );

    app.use(
        '/yourcode.js',
        createProxyMiddleware({
            target: 'https://kit.fontawesome.com/',
            secure:false,
            changeOrigin: true,
        })
    );


   app.use(cors());

};
