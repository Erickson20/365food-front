import {React, useEffect} from "react";
import Routers from "./router"
import './App.css';
import { Route, Switch, BrowserRouter } from "react-router-dom";

import Echo from 'laravel-echo';
window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'local',
    wsHost:'192.241.154.148',
    wsPort: 6001,
    forceTLS: false,
    disableStats: true
});


function App() {

  useEffect(()=>{
  },[Routers])


  return (

              <BrowserRouter>
                  <Switch>

                      {
                          Routers.map((data,i)=>
                              <Route path={data.path} key={i}   exact={data.exact} render={ props =>(

                                  <data.component props={props}/>

                              )} />
                          )
                      }
                  </Switch>
              </BrowserRouter>

  );

}

export default App;
