import api from '../../api/create';
import axios from 'axios'

function getClientes() {

    return api.get('/clients')

}


function createClient(body) {

    return api.post('/clientes',body)

}

export {
    getClientes,
    createClient
}
