import api from '../../api/create';




function getOrders() {

    return api.get('/orders/pending')

}

function updateStatusOrders(body,id) {

    return api.put('/orders/'+id+'/tracking',body)

}


export {
    getOrders,
    updateStatusOrders
}
