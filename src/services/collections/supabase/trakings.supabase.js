
import {getSupabase} from "../../api/useSupabase"



async function createOrder(order){

    await (await getSupabase())
        .from("trakings")
        .insert([{
            orderNumber: order.orderNumber,
            status: 'Pendiente',
            client: order.client ,
            phone: order.phone,
            address: order.address}]);

}

async function updateOrderStatus(orderNumber, status){
    const { data, error } = await (await getSupabase())
        .from('trackings')
        .update({ status: status })
        .match({ orderNumber: orderNumber })
    return !error? data: false
}

async function fetchAllOrdersTrakings(){
    const { data, error } = await (await getSupabase())
        .from('trackings')
        .select()
    return !error? data: false
}

async function subscribeTrakings(){
    await (await getSupabase())
        .from('tracking:orderNumber=eq.')
        .on('*', payload => {
            console.log('track',payload)
        }).subscribe();
}



export {
    createOrder,
    updateOrderStatus,
    subscribeTrakings,
    fetchAllOrdersTrakings
};
