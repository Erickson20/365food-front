

import {getSupabase} from "../../api/useSupabase"

async function createUser(userData) {
    const { user, session, error } = await (await getSupabase()).auth.signUp(userData)

    return {user,session,error}

}

async function loginUser(userData) {
    const { user, session, error } = await (await getSupabase()).auth.signIn(userData)

    //fetchAllOrdersTrakings()
    return {user,session,error}
}

async function logout() {
  await (await getSupabase()).auth.logout();
}





export {
    createUser,
    loginUser,
    logout
};
