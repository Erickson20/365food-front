import api from '../../api/create';




function saveInvoice(body) {

    return api.post('/orders',body)

}

function saveLineInvoice(body) {

    return api.post('/lineafacturaclientes',body)

}

function saveRecibo(body) {

    return api.post('/reciboclientes',body)

}



export {
    saveInvoice,
    saveLineInvoice,
    saveRecibo
}
