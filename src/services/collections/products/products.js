import api from '../../api/create';




function getProducts() {

    return api.get('/products')

}

function getProductsNoAuth() {

    return api.get('/products/noauth')

}



function getStocks() {

    return api.get('/stocks')

}

function updateStock(id,body) {

    return api.put('/stocks/'+id,body)

}

export {
    getProducts,
    updateStock,
    getStocks,
    getProductsNoAuth
}
