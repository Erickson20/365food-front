import axios from 'axios'

import config from '../../config';
import  {alert} from '../../helpers/helpers'
/**
 * Create an Axios Client with defaults
 */
const api = axios.create({
    baseURL: config.api,
    crossDomain: true,
    withCredentials: false,
    headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods':'GET, POST, PUT, DELETE',
        'Token':config.Token,
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
    }
});


api.interceptors.response.use(response => {
    return response;
}, error => {
    console.log({error})
    if (error.response.status === 4021) {
        window.location.href = '/login';
        localStorage.removeItem('token');
    }
    return error;
});
export default api;
