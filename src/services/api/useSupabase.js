

import Config from "../../config"

let supabase = null;
async function getSupabase() {

  if (supabase) return supabase;

  const { createClient } = await import("@supabase/supabase-js");

  supabase = createClient(Config.supabaseUrl,Config.supabaseKey);

  return supabase;
}


export {
	getSupabase
}





