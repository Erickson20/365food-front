import * as Users from "./collections/users/users";
import * as Product from "./collections/products/products";
import * as Category from "./collections/category/category";

import * as Clients from "./collections/clients/clients";
import * as Invoice from "./collections/invoice/invoice";
import * as Orders from "./collections/orders/orders";
import * as Supabase from  './collections/supabase/index'

export {
    Users,
    Product,
    Category,
    Clients,
    Supabase,
    Invoice,
    Orders,
};
