
import React,{useEffect,useState,useContext} from "react"
import Login from "../login/login";
import CardSelected from "../../components/cardSelected/cardSelected";
import Header from "../../components/header/header"
import "./main.css"
import { useHistory } from "react-router-dom";
import Button from "../../components/button/button";

function Main() {
    const history = useHistory();

    return (
        <>
            <Header text={'SELECCIONE UNA TIPO DE ORDEN'}/>
            <div className="center-main-card">
                <CardSelected text={'ORDEN PARA PEDIDOS'} onClick={()=>history.push({
                    pathname: '/payment',
                    state: {
                        options: 'delivery'
                    }
                })} iconName={"delivery_dining"}/>
                <Button
                    onCLick={()=> history.push('/board')}
                    type={null}
                    style={'button-sencond'}
                    size={10}
                    icon={'home'}
                    text={'ATRAS'}/>
                <CardSelected text={'ORDEN EN EL LOCAL'} onClick={()=>history.push({
                    pathname: '/payment',
                    state: {
                        options: 'local'
                    }
                })} iconName={"other_houses"}/>
            </div>
        </>

    );
}

export default Main;
