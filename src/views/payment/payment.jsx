
import React,{useEffect,useState,useContext} from "react"
import Login from "../login/login";
import CardSelected from "../../components/cardSelected/cardSelected";
import Header from "../../components/header/header"
import { useHistory } from "react-router-dom";
import { useLocation } from "react-router-dom";
import Button from "../../components/button/button";
function Payment() {
    const history = useHistory();
    const location = useLocation();
    return (
        <>
            <Header text={'SELECCIONE UN TIPO DE PAGO'}/>

            <div className="center-main-card">
                <CardSelected text={'EN EFECTIVO'} onClick={()=>history.push({
                    pathname: '/menu',
                    state: {
                        payment: 'efectivo',
                        options: location.state.options

                    }
                })} iconName={"monetization_on"}/>

                <Button
                    onCLick={()=> history.push('/board')}
                    type={null}
                    style={'button-sencond'}
                    size={10}
                    icon={'home'}
                    text={'ATRAS'}/>
                <CardSelected text={'EN TRANSFERENCIA'} onClick={()=>history.push({
                    pathname: '/menu',
                    state: {
                        payment: 'transferencia',
                        options: location.state.options
                    }
                })} iconName={"price_change"}/>
            </div>
        </>

    );
}

export default Payment;
