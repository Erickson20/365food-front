import "./client.css"
import Header from "../../components/header/header"
import React, {useEffect, useState} from "react";
import Button from "../../components/button/button";
import { useHistory } from "react-router-dom";
import ClientBox from "../../components/clientBox/clientBox"
import Input from "../../components/input/input";
import { useLocation } from "react-router-dom";
import {alert,formatDataForApi} from "../../helpers/helpers";
import {ClientModel} from "../../models/index"
import Loading from "../../components/loading/loading";
import  Modal from "../../components/modal/modal"
function Order() {
    const history = useHistory();
    const location = useLocation();
    const [clients,setClients] = useState([]);
    const [saveClient, setSaveClient] = useState({})
    const [selectedClients, setSelectedClient] = useState({})
    const [modalOpen, setModalOpen] = useState(false)



    useEffect(async ()=>{
        if(location.state.client){
            setSelectedClient(location.state.client)
        }
        await getClients()
    },[])




    function formCreateNewClient(){
        return (
            <div>
                <br/>
                <Input iconName={'account_circle'} padding={10} width={'94%'} onChange={setValue} bkColor={'#f3f3f3'} name={'nombre'}   placeholder={'Nombre completo'}/><br/>
                <Input iconName={'apartment'}  padding={10} width={'94%'}  onChange={setValue} bkColor={'#f3f3f3'} name={'razonsocial'}  placeholder={'Nombre del la empresa'}/><br/>
                <Input iconName={'phone_in_talk'} padding={10} width={'94%'}  onChange={setValue} bkColor={'#f3f3f3'} name={'telefono1'}  placeholder={'Telefono'}/><br/>
                <br/>
                <div style={{display: 'flex',justifyContent: 'space-between'}}>
                    <Button text={'Cerrar'} style={'button-sencond'} onCLick={()=>setModalOpen(false)} size={20}/>

                    <Button text={'Guardar'} size={30} onCLick={()=> saveClientmethod()}/>
                </div>
            </div>
        )
    }


    async function saveClientmethod() {
        let body = saveClient
        body.cifnif = "ninguno";

        if(!body.nombre || !body.telefono1) return alert('warning','Hay campos obligatorio','nombre y telefono son obligatorio')

        let data = formatDataForApi(body)
        const result = await ClientModel.createClient(data)

        if(result.ok){
            alert('success','Dato guardado Correctamente','Puede continuar...')
            await getClients()
            setModalOpen(false)
        }
        if(!result.ok){
            alert('danger','Ups!, lo sentimos','no se pudo guardar los datos correctamente')
        }

    }

    function setValue(e) {
        let saveClientArray = saveClient;
        saveClientArray[e.target.name] = e.target.value

        setSaveClient(saveClientArray);

    }




    async function getClients() {
        const result = await ClientModel.getClients();
        console.log('result all client', result)
        if(result && result.data.length>0){
            setClients(result.data)
        }

    }


    async function searchClient(e){

        if(e.target.value === ""){
            return await getClients()
        }
        let result = clients.filter((data,i)=>{
            return data.nombre.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
        })

        if(result.length===0){
            result = clients.filter((data,i)=>{
                return data.telefono1.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
            })
        }
        setClients(result)
    }



    function sendToOrder() {

        let products = location.state.products || null;

                if (!selectedClients.nombre) {
                    return alert(
                        'warning',
                        'Ups!, tienes que seleccionar un cliente',
                        'Favor de selecionar un cliente para continuar'
                    )
                }
                    history.push({
                        pathname: '/order',
                        state: {
                            options: location.state.options,
                            products: products,
                            payment: location.state.payment,
                            client: selectedClients
                        }
                    })
    }
    return (
        <>
            <Header text={'INFORMACION DEL CLIENTE'}/>
            <div className={'center-main'}>
                <div className={'header-button-client'}>
                    <Button
                        onCLick={()=> history.push({
                            pathname: '/cart',
                            state: location.state
                        })}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'ATRAS'}/>
                    <Button
                        onCLick={()=> history.push('/board')}
                        type={null}
                        style={'button'}
                        size={10}
                        text={'INICIO'}/>
                    <Button
                        onCLick={()=> sendToOrder()}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'SIGUIENTE'}/>
                </div>
                <div className={"card-container-client"}>

                    <Input iconName={"search"} onChange={searchClient} bkColor={'#f3f3f3'}  placeholder={"Buscar el cliente por numero o nombre"} typeInput={"text"}/>

                    {clients.length>0&&clients.map((data,i)=>
                        <ClientBox selected={data.nombre === selectedClients.nombre} onClick={ ()=>setSelectedClient({nombre:data.nombre,telefono:data.telefono1, codclient: data.codcliente})} text={data.nombre} phone={data.telefono1}/>
                    )}
                    {clients.length===0&&(
                        <Loading load={true}/>
                    )}
                </div>
            </div>

            {/*========================== body modal ============================*/}
            <Modal contenlabel={'hola mundo'} title={'CREAR UN NUEVO CLIENTE'} openModal={()=>setModalOpen(false)} modalIsOpen={modalOpen} content={formCreateNewClient()}/>

        </>
    );
}

export default Order;
