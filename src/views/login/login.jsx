

import Button from "../../components/button/button.jsx"
import Input from "../../components/input/input"
import Logo from "../../img/logo.png"
import "./login.css"
import React,{useEffect,useState,useContext} from "react"
import { UsersModel } from "../../models/index";
import { useHistory } from "react-router-dom";
import {alert} from "../../helpers/helpers"
function Login() {
    const history = useHistory();
    const [user, setUser] = useState({ email: "", password: ""})


    const HandleLogin = async ()=>{
       const result = await UsersModel.login(user)
       localStorage.removeItem('token')
        console.log(result)
       if(result.access_token){
           localStorage.setItem('token',result.access_token)
           history.push('/board')
       }

        if(result.error){
            alert('warning','ups!, el usuario o contraseña son incorrectos', 'Favor de verificar las credenciales')
        }


    }
    const setValue = (e) =>{
        let body = user;
        body[e.target.name] = e.target.value;
        setUser(body);
    }

    const handleSignup = async () => {

    }



    return (
        <div className="center ">

            <i className="material-icons icon-login-bk1  floating ">lunch_dining</i>
            <i className="material-icons icon-login-bk2 floating ">sports_bar</i>
            <div className={"card card-animation"}>
                <img src={Logo} className={"neon-imge"} width={250}/>
                <Input iconName={"person"} name={'email'} onChange={(e) => setValue(e)}  placeholder={"Usuario"} typeInput={"text"}/><br/><br/>
                <Input iconName={"lock"} name={'password'} onChange={(e)=>setValue(e)}  placeholder={"password"} typeInput={"password"}/>
                <br/><br/>
                <Button
                    onCLick={()=> HandleLogin()}
                    type={null}
                    style={'button'}
                    text={'ENTRAR'}/>
            </div>
        </div>
    );
}

export default Login;
