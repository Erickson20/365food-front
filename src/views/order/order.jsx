import "./order.css"
import Header from "../../components/header/header"
import React, {useEffect,useState} from "react";
import Button from "../../components/button/button";
import { useHistory } from "react-router-dom";
import { useLocation } from "react-router-dom";
import {setproducts} from "../../helpers/helpers";
import {alert} from "../../helpers/helpers"
import {InvoiceModel, ProductModel} from "../../models/index"
import Loading from "../../components/loading/loading";

function Order() {
    const history = useHistory();
    const location = useLocation();
    const [observaciones, setObservaciones] = useState("")
    const [loading, setLoading] = useState(false)
    const [total,setTotal] = useState(0)
    console.log({location})


    function Today() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


        today = dd + '-' + mm + '-' + yyyy;
        return {date:today,time:time}

    }
    useEffect(()=>{
        TotalOrder()
    },[])

    function TotalOrder() {
        let products = location.state.products&& Object.values(location.state.products)
        let total = products.map((data,i)=> data.price * data.count).reduce((a, b) => a + b, 0)
        setTotal(total)
    }

    async function saveOrder(){
        setLoading(true)
        let data = location.state
        let bodyFormat = {}
        bodyFormat.observaciones = observaciones;
        bodyFormat.total = total;
        bodyFormat.codcliente = data.client.codclient;
        bodyFormat.nombrecliente =  data.client.nombre;
        bodyFormat.neto = total;
        bodyFormat.netosindto = total;
        bodyFormat.codpago = data.payment === 'efectivo'?"CONT":"TRANS";
        bodyFormat.items = setproducts(location.state.products)
    //=========== save invoice format=============
        console.log({bodyFormat})
        let result = await InvoiceModel.saveInvoice(bodyFormat)
        console.log({result})
        if(!result.success){
            setLoading(false)
            return alert(
                'warning',
                'Ups!, paso algo mientras se guardaba',
                "Tenemos un error al guardar la factura, favor de contactar a tu administrador "
            )
        }
        if(result.success){
            setLoading(false)
            alert(
                'success',
                'SE CREO UNA NUEVA ORDEN CON ID:'+result.data.codigo,
                'Esta orden fue creada el dia '+Today().date+' a las '+Today().time
            )
            history.push('/board')

        }
    }


    return (
        <>
            <Header text={'DETALLE DE LA ORDEN'}/>
            <div className={'center-main'}>
                <div className={'header-button'}>
                    <Button
                        onCLick={()=> history.push({
                            pathname: '/client',
                            state: location.state
                        })}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'ATRAS'}/>
                    <Button
                        onCLick={()=> history.push('/board')}
                        type={null}
                        style={'button'}
                        size={10}
                        text={'INICIO'}/>
                    <Button
                        onCLick={()=> saveOrder()}
                        type={null}
                        style={'button-sencond'}
                        size={20}
                        text={'CREAR ORDEN'}/>
                </div>
                <div className={"card-container-cart"}>

                    <div style={{display: 'flex', flexDirection: 'column', width: '100%'}}>
                        <Loading load={loading}/>
                        {!loading&&(
                            <>
                                <h2>Informacion de Cliente:</h2>
                                <hr style={{width:'100%'}}/>
                                <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                    <label>Nombre:</label>
                                    <h3 style={{color: '#d00000'}}>{location.state.client.nombre}</h3>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                    <label>Telefono:</label>
                                    <h3 style={{color: '#d00000'}}>{location.state.client.telefono}</h3>
                                </div>

                                <h2>Informacion de Productos:</h2>
                                <hr style={{width:'100%'}}/>
                                {Object.values(location.state.products).map((data,i)=>
                                    <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                        <label>{data.text} - {data.price} RD$ x {data.count} </label>
                                        <h3 style={{color: '#d00000'}}>{data.price * data.count} RD$</h3>
                                    </div>
                                )}

                                <h2>Detalle de la Orden:</h2>
                                <hr style={{width:'100%'}}/>
                                <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                    <label>Total de la Orden:</label>
                                    <h3 style={{color: '#d00000'}}>{total} RD$</h3>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                    <label>Tipo de Orden:</label>
                                    <h3 style={{color: '#d00000'}}>{location.state.options}</h3>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                    <label>Tipo de Pago:</label>
                                    <h3 style={{color: '#d00000'}}>{location.state.payment}</h3>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between', backgroundColor: 'white', padding: 5, borderRadius:10}}>
                                    <label>Hora:</label>
                                    <h3 style={{color: '#d00000'}}>{Today().date + ' ' +Today().time}</h3>
                                </div>
                                <label>Observaciones:</label><br/>
                                <textarea onChange={(e)=>setObservaciones(e.target.value)} rows={8} style={{borderRadius: 5}}>
                        </textarea>

                            </>
                        )}
                    </div>
                </div>
            </div>

        </>
    );
}

export default Order;
