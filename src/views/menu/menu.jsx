
import React,{useEffect,useState,useContext} from "react"
import Header from "../../components/header/header"
import "./menu.css"
import { useHistory } from "react-router-dom";
import Product from "../../components/product/product";
import {alert,alertDecisions} from "../../helpers/helpers"
import Button from "../../components/button/button";
import { useLocation } from "react-router-dom";
import {ProductModel} from "../../models/index"
import Loading from "../../components/loading/loading";
function Menu() {
    const history = useHistory();
    const location = useLocation();
    const [count,setCount] = useState([])
    const [products, setProducts] = useState([])


    useEffect(async ()=>{
        const result = await ProductModel.getProducts()
        console.log({result})
       setProducts(result.categories)
        location.state.products && setCount(location.state.products)
        console.log('into use Effect menu',location,Object.keys(count))
    },[location])



    function filterProducts() {
        for (const key in count) {
            if(count[key].count===0){
                delete count[key];
            }
        }
        if(Object.keys(count).length === 0) {
            return alert(
                'warning',
                'Ups!, no tienes productos en el carrito',
                'Favor de introducir la cantidad de por lo menos un producto'
            )
        }
        history.push({
            pathname: '/cart',
            state: {
                options: location.state.options,
                payment: location.state.payment,
                products: count
            }
        })
    }

    return (
        <>
            <Header text={'MENU'}/>


            <div className={'center-main'}>
                <div className={'header-button'}>
                    <Button
                        onCLick={()=> history.push({
                            pathname: '/payment',
                            state: location.state
                        })}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'ATRAS'}/>
                    <Button
                        onCLick={()=> history.push('/board')}
                        type={null}
                        style={'button-sencond'}
                        size={10}
                        text={'INICIO'}/>
                    <Button
                        onCLick={()=> filterProducts()}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'SIGUIENTE'}/>
                </div>
                <div className={"card-container"}>
                    {products.length===0&&(
                        <Loading load={true}/>
                    )}
                    {products.map((data,i)=>
                        <>
                            <div className={'title-category'}>
                                <h1>{ (i+1)+'. '+data.name.toUpperCase()}</h1>
                            </div>
                            <di className={"card-menu"}>
                                {
                                    data.products.map((item,i)=>
                                        <Product
                                            text={item.descripcion}
                                            img={item.img}
                                            price={item.precio}
                                            stock={item.stockfis}
                                            idPropduct={item.idproducto}
                                            description={item.observaciones}
                                            setCount={setCount}
                                            location={location}
                                            count={count}
                                        />
                                    )

                                }
                            </di>

                        </>
                    )}

                </div>
            </div>
        </>

    );
};



export default Menu
