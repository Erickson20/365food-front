
import React,{useEffect,useState,useContext} from "react"
import Header from "../../components/header/header"
import "./menu.css"
import { useHistory } from "react-router-dom";
import Product from "../../components/product/product";
import {alert,alertDecisions} from "../../helpers/helpers"
import Button from "../../components/button/button";
import { useLocation } from "react-router-dom";
import {ProductModel} from "../../models/index"
import Loading from "../../components/loading/loading";
function Menu() {
    const history = useHistory();
    const location = useLocation();
    const [count,setCount] = useState([])
    const [products, setProducts] = useState([])


    useEffect(async ()=>{
        const result = await ProductModel.getProductsNoAuth()
        setProducts(result.categories)
        console.log(result.categories)
        //location.state.products && setCount(location.state.products)
       // console.log('into use Effect menu',location,Object.keys(count))
    },[])



    

    return (
        <>
            <Header cls="between-header" text={'MENU'}/>


            <div style={{ display: 'flex', flexDirection:'column', justifyContent:'center', paddingRight: '5%',paddingLeft: '5%'}} >
                {products.map((data,i)=>
                    data.name.toUpperCase() !== 'INGREDIENTES'&&(
                        <>
                            <div style={{margin:0, height:65, backgroundColor:'#a90303',color:'white', borderRadius: 20}} >
                                <h1><center>{data.name}</center></h1>
                            </div>

                            <div style={{width:'95%'}}>
                                {data.products.map((p,it)=>
                                
                                <div >
                                    {console.log(window.screen.width)}
                                    <div style={{display:'flex', justifyContent: 'space-between', padding:10}}>
                                        <div style={{ display:'flex',flexDirection:window.screen.width<500?'column':'row'}}>
                                            <img className="responsive" src={p.img?p.img:'https://res.cloudinary.com/dbzzxyrze/image/upload/v1593819177/unnamed_ra4dzj.png'} />
                                            &nbsp;&nbsp;&nbsp;
                                            <div style={{color:'white', alignItems:'center',width:window.screen.width<500?'100%':'50%'}}>
                                                {window.screen.width<500?(
                                                   <div>
                                                        <label style={{color:'orange'}}>{p.descripcion.toUpperCase()}</label><br/>
                                                        <label style={{color:'white', textAlign:'justify'}}>{p.observaciones.replace(/^\w/, (c) => c.toUpperCase())}</label><br/><br/>
                                                        <label style={{color:'#a90303'}}><b>$RD {p.precio}</b></label>
                                                   </div>
                                                ):(
                                                    <div>
                                                        <h3 style={{color:'orange'}}>{p.descripcion.toUpperCase()}</h3>
                                                        <h5 style={{color:'white',textAlign:'justify'}}>{p.observaciones.replace(/^\w/, (c) => c.toUpperCase())}</h5>
                                                        <h3 style={{color:'#a90303'}}>$RD {p.precio}</h3>
                                                    </div>
                                                )
                                                }
                                                
                                            </div>
                                        </div>
                                        <div style={{color:'white', width:'20%',alignItems:'center'}}>
                                            
                                        </div>
                                </div>
                                <hr style={{width:'100', margin: 0}}/>
                                </div>
                                )}
                            </div>
                            <br/>
                        </>
                    )
                )}
            </div>
        </>

    );
};



export default Menu
