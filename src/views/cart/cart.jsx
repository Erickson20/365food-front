import "./cart.css"
import Header from "../../components/header/header"
import React,{useEffect,useState} from "react";
import Button from "../../components/button/button";
import { useHistory } from "react-router-dom";
import CartBox from "../../components/cartBox/cartBox";
import {alert,alertDecisions} from "../../helpers/helpers"
import { useLocation } from "react-router-dom";
function Cart() {
    const history = useHistory();
    const location = useLocation();
    const [update, setUpdate] = useState(false);


    useEffect(()=>{
        let products = location.state.products
    },[update])


    async function deleteproduct(id){
        let response = await alertDecisions(
            'warning',
            'Esat seguro de eliminar este producto?',
            'Al eliminar el producto tendra que volver a la pantalla de menu'
        )
        if(response){
            delete location.state.products['product'+id]
            setUpdate(true)
        }

    }

    function sendToClient() {

        let products = location.state.products || null;
        if(products) {
            if (Object.keys(products).length === 0) {
                return alert(
                    'warning',
                    'Ups!, no tienes productos en el carrito',
                    'Favor de introducir la cantidad de por lo menos un producto'
                )
            }
            history.push({
                pathname: '/client',
                state: {
                    options: location.state.options,
                    payment:location.state.payment,
                    products: products
                }
            })
        }

    }


    return (
        <>
            <Header text={'TU CARRITO '}/>
            <div className={'center-main'}>
                <div className={'header-button'}>
                    <Button
                        onCLick={()=> history.push({
                            pathname: '/menu',
                            state: location.state
                        })}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'ATRAS'}/>
                    <Button
                        onCLick={()=> history.push('/board')}
                        type={null}
                        style={'button-sencond'}
                        size={10}
                        text={'INICIO'}/>
                    <Button
                        onCLick={()=> sendToClient()}
                        type={null}
                        style={'button'}
                        size={20}
                        text={'SIGUIENTE'}/>
                </div>
                <div className={"card-container-cart"}>
                    {console.log(Object.values(location.state.products))}
                    {Object.keys(location.state.products).length>0&&Object.values(location.state.products).map((item,i)=>
                        <CartBox text={item.text} idProduct = {item.idPropduct} img={item.img} price={(item.price*item.count)} deleteMethod={deleteproduct} count={item.count} />
                    )}
                    {Object.keys(location.state.products).length===0&&(
                        <h1 className={'center'}>no tiene productos en tu carrito :(...</h1>
                    )}
                </div>
            </div>

        </>
    );
}

export default Cart;
