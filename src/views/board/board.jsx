import Board, { moveCard } from '@lourenci/react-kanban'
import '@lourenci/react-kanban/dist/styles.css'
import {useHistory} from "react-router-dom";
import Header from "../../components/header/header";
import "./board.css";
import Button from "../../components/button/button";
import React, {useEffect, useState} from "react";
import CardBox from "../../components/cardBoard/cardBoard";
import {OrdersModel as Orders} from "../../models/index";
import {setProductTckings,nameForStatus} from "../../helpers/helpers"


function BoardMain() {
    const history = useHistory();
    const [orders,setOders] = useState([])
    const [ordersApi, setOrdersApi] = useState([])
    useEffect( async ()=>{
        const result = await Orders.getOrders()
       let dataForBoard = setProductTckings(result.orders)
        console.log('ordernes api', dataForBoard)
        setOders(dataForBoard)
        setOrdersApi(result.orders)


    },[])

    useEffect(()=>{
        window.Echo.channel('kitchen')
            .listen('OrderTrackingUpdatedForKitchen', (e) =>{
                console.log(`e`, e)
                let allOrders =  setProductTckings([e.order])
                setOders([]);
                setOders(allOrders);
                
            })

    },[])

    function newOrderCard(order) {
        console.log(`order`, order,ordersApi )
            for (const i in ordersApi) {
                if (ordersApi[i].orderNumber === order.orderNumber) {
                    ordersApi[i].orderNumber = order.orderNumber
                }
            }
            return ordersApi
        
    }


    async function handleCardMove(_card, info, source, destination) {
        const status = nameForStatus(destination.toColumnId)
        //const result = await Orders.updateStatusOrders({status},info.orderNumber)
        
    }



    return (
        <>
            <Header text={'PANTALLA DE ORDENES'}/>
            <div className={'header-button-board'}>
                <Button
                    onCLick={()=> history.push('/main')}
                    type={null}
                    style={'button-sencond'}
                    size={20}
                    text={'NUEVA ORDEN'}/>
                    <div style={{width: '3%'}}>

                    </div>

            </div>
            {orders.columns&&(
                <Board
                    initialBoard={orders}
                    onCardDragEnd={handleCardMove}
                    disableColumnDrag
                    renderCard={({ orderNumber }, { dragging }) => (
                        <CardBox text={orderNumber} dragging={dragging} />
                    )}
                >
                    {orders}
                </Board>
            )}
        </>
    );
};

export default BoardMain;
