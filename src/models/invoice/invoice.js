import * as Services from "../../services/index"
import * as Helpers from "../../helpers/helpers"



async function saveInvoice(body) {
    const result = await Services.Invoice.saveInvoice(body)
    return result.data

}


async function saveLineInvoice(body) {
    const result = await Services.Invoice.saveLineInvoice(body)
    return result.data

}

async function saveRecibo(body) {
    const result = await Services.Invoice.saveRecibo(body)
    return result.data

}




export {
    saveInvoice,
    saveLineInvoice,
    saveRecibo
}
