import * as Services from "../../services/index"




async function getOrders() {
    const result = await Services.Orders.getOrders()
    return result.data
}


async function updateStatusOrders(body,id){
    const result = await Services.Orders.updateStatusOrders(body,id)
    return result.data
}

export {
    getOrders,
    updateStatusOrders
}

