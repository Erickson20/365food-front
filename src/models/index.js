import * as ProductModel from "./products/products"
import * as CategoryModel from "./category/category"
import * as ClientModel from "./clients/clients"
import * as InvoiceModel from "./invoice/invoice"
import * as UsersModel from "./users/users"
import * as OrdersModel from "./orders/orders"



export {
    ProductModel,
    CategoryModel,
    ClientModel,
    InvoiceModel,
    UsersModel,
    OrdersModel
};
