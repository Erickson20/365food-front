import * as Services from "../../services/index"
import * as Helpers from "../../helpers/helpers"



async function getProducts() {
    const result = await Services.Product.getProducts()
    return result.data

}

async function getProductsNoAuth() {
    const result = await Services.Product.getProductsNoAuth()
    return result.data

}

async function getStocksByProduct(idProduct) {
    const result = await Services.Product.getStocks()
    const stock = result.data.filter((data,i)=>{
        return data.idproducto ===idProduct;
    })
    return stock[0].idstock
}

async function updateStock(id,body) {
    const result = await Services.Product.updateStock(id,body)
    return result.data

}





export {
    getProducts,
    getStocksByProduct,
    updateStock,
    getProductsNoAuth
}

