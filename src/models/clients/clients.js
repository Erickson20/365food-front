import * as Services from "../../services/index"


async function getClients() {
    const result = await Services.Clients.getClientes()
    return result.data
}


async function createClient(body) {
    const result = await Services.Clients.createClient(body)
    return result.data
}



export {
    getClients,
    createClient
}

