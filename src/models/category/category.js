import * as Services from "../../services/index"
import * as Helpers from "../../helpers/helpers"

async function getCategory(id) {
    const result = await Services.Category.getFamilias(id)
    return result.data
}


async function getCategories() {
    const result = await Services.Category.getCategories()
    return result.data
}



export {
    getCategory,
    getCategories
}

