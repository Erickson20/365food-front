import * as Services from "../../services/index"




async function login(body) {
    const result = await Services.Users.login(body)
    return result.data
}



export {
    login
}

