
import Clasic from "./img/clasic.png"
import Especial from "./img/popi.png"

const products = [


    {
        name:'burger',
        type:'burger',
        products: [
            {
                id: 1,
                path: '/clasic',
                img: Clasic,
                name: 'clasi',
                description: 'hamburguesa con una carne artesanal queso y tomate',
                price: '200',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
        ]
    },
    {
        name:'yaroa',
        type:'yaroa',
        products:[
            {
                id: 1,
                path: '/clasic',
                img: Clasic,
                name: 'clasi',
                description: 'hamburguesa con una carne artesanal queso y tomate',
                price: '200',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
            {
                id: 1,
                path: '/popi',
                img: Especial,
                name: 'popi',
                description: 'hamburguesa con doble carne artesanal, queso fundiod, huevo, salsa de la casa acompanado de papa fritas',
                price: '400',
                stock:3,
                exact: false,
                show:true
            },
        ]
    },

];


export  default  products
