import "./clientBox.css"
import Button from "../button/button";
import React from "react";
import imgAvatar from "../../img/avatar.jpeg"
function ClientBox({phone = 0,selected = false,description= null, img = null, placeholder= 'introduce el dato', text='text', onClick = null}) {
    return (
        <div style={{borderColor: selected?'orange':'#a90303', borderWidth: selected?8:5}} className={'card-product zoom-client '} onClick={onClick}>
            <img src={img || imgAvatar} className={'img-product'} alt={"producto"}/>
            <label style={{margin: 2}} ><b>{text.toUpperCase()}</b></label>
            <label style={{margin: 2}} >tel: {phone === ""?"no tiene":phone}</label>
            <div >
            </div>
        </div>
    );
}

export default ClientBox;
