import "./button.css"

function Button({onCLick = null, size = 50, type = null, text = null,disabled=false,style = 'button', icon = false, iconSize = 60}) {
    if(icon){
        return (
            <button disabled={disabled} className={style} style={{width: size + '%'}} onClick={onCLick}>
                <span className="material-icons icon-button" style={{fontSize: iconSize}}>{icon}</span>
            </button>
        );
    }

    if(!icon){
        return (
            <button disabled={disabled} className={style} style={{width: size + '%'}} onClick={onCLick}>
                {text}
            </button>
        );
    }
}

export default Button;
