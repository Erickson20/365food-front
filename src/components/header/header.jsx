import "./header.css"
import Logo from "../../img/logo.png"
import Button from "../button/button";
import React from "react";
function Header({text = 'ninguno', cls = 'center-header'}) {
    return (
        <div className={cls}>
           <h1 className={'title-header'}>{text}</h1>
            <img src={Logo} width={150}/>
        </div>
    );
}

export default Header;
