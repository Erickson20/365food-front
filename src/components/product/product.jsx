import "./product.css"
import Button from "../button/button";
import ImgClasic from "../../img/popi.png"
import {alert} from "../../helpers/helpers"
import React, {useEffect,useState} from "react";


function Product({price = 0,location = null,description= null, idPropduct = 0, stock = 0, img = null, count= [], text='text', setCount = null}) {

    if(!img) img = 'https://res.cloudinary.com/dbzzxyrze/image/upload/v1593819177/unnamed_ra4dzj.png';
    const [update,setUpdate] = useState(0)

    function setCountItem(data,operator){
        if(update === stock && operator){
            return alert(
                'warning',
                stock+' es la cantidad maxima de '+text+' que hay en el almacen',
                'Favor de actualizar el almacen si quiere agregar mas productos'
            )
        }
        if(isNaN(data)) data = 1;
        let selected = count
         selected['product'+idPropduct] = {
             price:price,
             text:text,
             count:data,
             description:description,
             img:img,
             stock:stock,
             idproducto:idPropduct
         }
        setUpdate(data);
        setCount(selected);

    }

    useEffect(()=>{
        if(location.state.products&&location.state.products['product'+idPropduct]){
            setUpdate(location.state.products['product'+idPropduct].count);
        }else{
            setUpdate(0)
        }

    },[location])
    return (
        <div className={'card-product '} >
           <img src={img} className={'img-product'}  alt={"producto"}/>
            <label style={{margin: 2}} ><b>{text.toUpperCase()}</b></label>
            <label className={'price-text'}>{price} $RD</label>
            <label className={'description-product'}>{description.substring(0,47)} ...</label>
            <div >
                <Button
                    onCLick={()=> setCountItem((!count['product'+idPropduct]?0:count['product'+idPropduct].count)-1,false)}
                    type={null}
                    disabled={update === 0}
                    style={'button-redounded'}
                    text={'ENTRAR'}
                    iconSize={20}
                    size={28}
                    icon={'remove_circle_outline'}/>
                { update}
                <Button
                    onCLick={()=> setCountItem((!count['product'+idPropduct]?0:count['product'+idPropduct].count)+1,true)}
                    type={null}
                    style={'button-redounded'}
                    text={'ENTRAR'}
                    iconSize={20}
                    size={28}
                    icon={'add_circle_outline'}/>
            </div>
        </div>
    );
}

export default Product;
