import "./input.css"

function Input({iconName = 'block',width = null, padding = 0, name="", bkColor = 'white', placeholder= 'introduce el dato', typeInput='text', onChange = null}) {
    return (
        <div className={'field-container '} style={{backgroundColor: bkColor, padding: padding, width:width}}>
            <i className="material-icons icon-login">{iconName}</i>
            <input type={typeInput} onChange={onChange} name={name} className="field" placeholder={placeholder}/>
        </div>
    );
}

export default Input;
