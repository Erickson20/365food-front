import "./cardSelected.css"

function CardSelected({iconName = 'block', placeholder= 'introduce el dato', text='text', onClick = null}) {
    return (
        <div className={'card-selected zoom'} onClick={onClick}>
            <i className="material-icons icon-main">{iconName}</i>
            <h1 className={"title-card"}>{text}</h1>
        </div>
    );
}

export default CardSelected;
