import "./cardBox.css"
import Button from "../button/button";
import React, {useEffect, useState} from "react";
import imgAvatar from "../../img/avatar.jpeg"
function CardBox({text = 'ninguno', onClick = null,dragging= null,moveCard=null}) {

    return (
        <div className={'card-order zoom-client '} onClick={onClick}>
            <div className={'card-info-client-header'}>
                <label><b>{text.toUpperCase()}</b></label>
                <span className="material-icons icon-button" style={{fontSize: 20}}>close</span>
            </div>
            <hr/>
            <div style={{width: '100%'}}>
                <div className={'card-info-client'}>
                    <label>Client:</label>
                    <label> <b>Rodriguez</b></label>
                </div>
                <div className={'card-info-client'}>
                    <label>Tel:</label>
                    <label><b>8097564233</b></label>
                </div>
                <div className={'card-info-client'}>
                    <label>Hora:</label>
                    <label><b>05:24 PM</b></label>
                </div>
                <div className={'card-info-client'}>
                    <label>Accion:</label>
                    <div>
                        <span className="material-icons icon-button" style={{fontSize: 20, paddingRight:15}}>edit</span>
                        <span className="material-icons icon-button" style={{fontSize: 20}}>info</span>
                    </div>
                </div>

            </div>

        </div>
    );
}

export default CardBox;
