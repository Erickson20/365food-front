import "./cartBox.css"
import Button from "../button/button";
import React from "react";
function CartBox({price = 0,description= null, idProduct = null,deleteMethod = null, img = null, count= 0, text='text', onClick = null}) {

    return (
        <div className={'cart-box zoom-client '} onClick={onClick}>
            <img src={img} className={'img-product-cart'} alt={"producto"}/>
            <div className={'detail-cart'}>
                <h1 style={{margin: 2}} ><b>{text.toUpperCase()}</b></h1>
                <div className={'card-info-cart'}>
                    <h3 >x {count}</h3>
                    <h3 className={'price-text'}>{price} $RD</h3>

                </div>
                <Button
                    onCLick={()=> deleteMethod(idProduct)}
                    type={null}
                    style={'button-sencond'}
                    size={100}
                    text={'Borrar'}/>
            </div>

        </div>
    );
}

export default CartBox;
