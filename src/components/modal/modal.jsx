import React from 'react'
import swal from 'sweetalert'
import Modal from 'react-modal';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        width: '50%',
        height: 'auto'
    }
};



function ModalView({closeModal = null, title = "ninguno", onRequestClose = null,contenlabel = "example", content = null, openModal = null, modalIsOpen = false}) {





    return (
        <div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <h1 style={{display: 'flex', justifyContent: 'center'}}>{title}</h1>

                {content}
            </Modal>
        </div>
    );
}

export default ModalView;

