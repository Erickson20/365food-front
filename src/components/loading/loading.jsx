import "./loading.css"
import Button from "../button/button";
import React from "react";
import Logo from "../../img/logo.png"
function Loading({load = true}) {

    return (
        <div className={'div-center'}>
            {load&&(
                <>
                    <span className="material-icons rotating icons-rotate">autorenew</span>
                    <div><label style={{color:'#d00000'}}><b>Cargando....</b></label></div>
                </>
            )}
        </div>
    );
}

export default Loading;
