
import Login from "./views/login/login";
import Main from "./views/main/main"
import Menu from "./views/menu/menu";
import MenuClient from "./views/menu/menuClient";
import Cart from "./views/cart/cart"
import Order from "./views/order/order"
import Client from "./views/client/client"
import BoardMain from "./views/board/board"
import Payment from "./views/payment/payment";



const routers = [


    {
        path: '/login',
        component: Login,
        name: '404',
        exact: false,
        show:true
    },
    {
        path: '/menuclient',
        component: MenuClient,
        name: 'menuclient',
        exact: false,
        show:true
    },
    {
        path: '/board',
        component: BoardMain,
        name: 'board',
        exact: false,
        show:true
    },
    {
        path: '/payment',
        component: Payment,
        name: 'pay',
        exact: false,
        show:true
    },
    {
        path: '/cart',
        component: Cart,
        name: 'cart',
        exact: false,
        show:true
    },
    {
        path: '/main',
        component: Main,
        name: 'home',
        exact: false,
        show:true
    },
    {
        path: '/menu',
        component: Menu,
        name: 'menu',
        exact: false,
        show:true
    },
    {
        path: '/order',
        component: Order,
        name: 'orden',
        exact: false,
        show:true
    },

    {
        path: '/client',
        component: Client,
        name: 'cliente',
        exact: false,
        show:true
    },
    {
        path: '/',
        component: Login,
        name: '404',
        exact: false,
        show:true
    },
];


export  default  routers
