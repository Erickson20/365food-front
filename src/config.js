//=================================== CHANGE ENVIRONMENT ==================================
const option = 'development'
//=========================================================================================

function env(env) {
    switch (env) {
        case 'local':
            return {
                urlFs:'http://localhost:83/facturascripts/api/3',
                tokenFs:'5uN0HUdtFZ8alIOzi14p',
                secret: 'palolo',
                socket: 'http://192.241.154.148:6001',
                urlSupabase: 'https://bibzlggwpprwqphqajez.supabase.co',
                keySupabase:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYyMDQzNzk2MiwiZXhwIjoxOTM2MDEzOTYyfQ.CB154iNC0lJQP6cCYxrBZ9yphHxF54ANEjSF0nwxoP4'
            }
        case 'development':
             return {
                 urlFs:'http://192.241.154.148:8001/api/v1',
                 secret: 'palolo',
                 socket: 'http://192.241.154.148:6001',
                 tokenFs:'5uN0HUdtFZ8alIOzi14p',
                 urlSupabase: 'https://bibzlggwpprwqphqajez.supabase.co',
                 keySupabase:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYyMDQzNzk2MiwiZXhwIjoxOTM2MDEzOTYyfQ.CB154iNC0lJQP6cCYxrBZ9yphHxF54ANEjSF0nwxoP4'
            }
        default:
            return {
                urlFs:'http://localhost:83/facturascripts/api/3',
                tokenFs:'5uN0HUdtFZ8alIOzi14p',
                socket: 'http://192.241.154.148:6001',
                secret: 'palolo',
                urlSupabase: 'https://bibzlggwpprwqphqajez.supabase.co',
                keySupabase:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYyMDQzNzk2MiwiZXhwIjoxOTM2MDEzOTYyfQ.CB154iNC0lJQP6cCYxrBZ9yphHxF54ANEjSF0nwxoP4'
            }
    }
}

module.exports ={
    api:          env(option).urlFs,
    supabaseUrl:  env(option).urlSupabase,
    supabaseKey:  env(option).keySupabase,
    SOCKET:       env(option).socket,
    SECRET_TOKEN: env(option).secret,
    Token:        env(option).tokenFs,
}
